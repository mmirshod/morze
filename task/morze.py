def code_morze(value):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    abc = {
        'A': '.-',
        'B': '-...',
        'C': '-.-.',
        'D': '-..',
        'E': '.', 
        'F': '..-.', 
        'G': '--.', 
        'H': '....', 
        'I': '..', 
        'J': '.---', 
        'K': '-.-', 
        'L': '.-..', 
        'M': '--', 
        'N': '-.', 
        'O': '---', 
        'P': '.--.', 
        'Q': '--.-', 
        'R': '.-.', 
        'S': '...', 
        'T': '-', 
        'U': '..-', 
        'V': '...-', 
        'W': '.--', 
        'X': '-..-', 
        'Y': '-.--', 
        'Z': '--..',
        '1': '.----',
        '2': '..---', 
        '3': '...--',
        '4': '....-',
        '5': '.....', 
        '6': '-....',
        '7': '--...', 
        '8': '---..', 
        '9': '----.', 
        '0': '-----', 
        ', ': '--..--', 
        '.': '.-.-.-', 
        '?': '..--..', 
        '/': '-..-.', 
        '-': '-....-', 
        '(': '-.--.', 
        ')': '-.--.-'
    }

    words = value.strip().split()
    morze_words = []
    for word in words:
        word = word.upper()
        morze_word = ""
        for ch in word:
            morze_word += f"{abc[ch]} "
        morze_words.append(morze_word)

    res = "".join(morze_words)
    return res[:-1]